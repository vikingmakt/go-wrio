# WRIO

Make sure to have **Go 1.9** installed and to have set **$GOPATH** correctly

```
$ go get gitlab.com/vikingmakt/go-wrio
$ make
$ ./wrio
```


## Headers

| Header            | Description                                           |
| ------            | -----------                                           |
| w-path            | URI path without any trailing or starting "/"         |
| w-method          | HTTP method                                           |
| w-query           | Raw URL query string                                  |
| w-request-uid     | Unique ID of the HTTP request (can be used for logging) |
| w-segments-length | How many segments in URI                              |
| w-segment-{n}     | The {n}th segment in the URI                          |
| w-h-{H}           | The {H} HTTP request header (i.e. "w-h-Content-Type") |
| w-r-h-{H}         | The {H} HTTP response header                          |