package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"strconv"
	"encoding/json"
	"io/ioutil"

	"github.com/glhrmfrts/tyr/rmq"
	"github.com/glhrmfrts/tyr/rfour"
	"github.com/glhrmfrts/tyr/raid"
)

const (
	exchangeTopic        = "wrio"
	responseHeaderPrefix = "w-r-h-"
)

var (
	conn *rmq.RMQ
	io   *rfour.IO
)

func handleRequest(w http.ResponseWriter, r *http.Request) {
	var body []byte
	if r.Method != "GET" {
		data := make([]byte, 1024)
		size, _ := r.Body.Read(data)
		body = data[0:size]
	}

	requestUID := raid.NewEtag().String()
	path := strings.Trim(r.URL.Path, "/")
	routingKey := strings.Replace(path, "/", ".", -1)
	headers := map[string]interface{}{
		"w-path": path,
		"w-method": r.Method,
		"w-query": r.URL.RawQuery,
		"w-request-uid": requestUID,
	}

	segments := strings.Split(path, "/")
	headers["w-segments-length"] = int32(len(segments))
	for i, segment := range segments {
		headers[fmt.Sprintf("w-segment-%d", i)] = segment
	}

	for key, value := range r.Header {
		headers[fmt.Sprintf("w-h-%s", key)] = value[0]
	}

	responseChannel, err := io.Request(
		exchangeTopic,
		routingKey,
		body,
		headers,
	)
	if err != nil {
		log.Println(err)
		w.WriteHeader(500)
		return
	}

	response := <-responseChannel
	w.Header().Add("x-w-request-uid", requestUID)

	ioErr, ok := response.Headers["__err__"]
	if ok && ioErr.(string) == "NOT_ROUTED" {
		w.WriteHeader(404)
		return
	}

	for key, value := range response.Headers {
		if strings.Contains(key, responseHeaderPrefix) {
			header := key[len(responseHeaderPrefix):]
			w.Header().Add(header, value.(string))
		}
	}

	if status, ok := response.Headers["w-status"]; ok {
		statusInt, _ := strconv.Atoi(status.(string))
		w.WriteHeader(statusInt)
	} else {
		w.WriteHeader(200)
	}
	w.Write(response.Body)
}

type Settings struct {
	Port string
	Amqp string
}

var (
	defaultSettings = Settings{
		Port: "8001",
		Amqp: "amqp://localhost:5672",
	}
	settings *Settings
)

func init() {
	content, err := ioutil.ReadFile("settings.json")
	if err != nil {
		log.Println("could not read settings:", err)
	}

	settings = &Settings{}
	*settings = defaultSettings
	if err := json.Unmarshal(content, settings); err != nil {
		log.Println("invalid settings json:", err)
	}
}

func main() {
	c, err := rmq.Connect(settings.Amqp)
	if err != nil {
		log.Fatal(err)
	}

	conn = c
	io = rfour.NewIO(c)

	http.HandleFunc("/", handleRequest)
	log.Fatal(http.ListenAndServe(":" + settings.Port, nil))
}
