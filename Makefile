OUT = wrio

$(OUT): main.go
	go build -o $(OUT)

clean:
	rm -rf $(OUT)

.PHONY: clean
